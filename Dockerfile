FROM docker.io/ruby:2.6.5
MAINTAINER Jeremy Thornhill <jnt6@duke.edu>

# Update system and Install extra packages
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - &&\
    apt-get upgrade -y &&\
    apt-get install -y nodejs \
                       yarn \
                       expect \
                       openssl \
                       less \
                       wget \
                       readline-common \
                       postgresql-common \
                       mariadb-client \
    # Chromium stuff is used for test phase *ONLY*, this should be ripped out at some point
                       chromium chromium-driver \
                       coreutils \
                       --no-install-recommends && \
    echo "" | /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh && \
    apt-get install -y postgresql-client-12 && \
    rm -rf /var/lib/apt/lists/*

ENV TZ America/New_York

# Set up our docker user
RUN echo 'docker-user:x:1000:0:docker container user:/home/docker-user:/bin/bash' >> /etc/passwd
RUN mkdir -p ~docker-user && chown -R docker-user:0 ~docker-user && chmod g+w ~docker-user

RUN umask 0002; mkdir /opt/app
RUN gem install bundler:2.0.2
ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1
RUN echo "gem: --no-document" > /root/.gemrc
COPY Gemfile /app/
COPY Gemfile.lock /app/
WORKDIR /app
# bundle does not respect the umask when run as a root user, sadly
RUN umask 0002; bundle install --without development --deployment && \
    chmod -Rf g+rwx /usr/local/bundle
COPY yarn.lock /app/
COPY package.json /app/
RUN umask 0002; bundle exec yarn install

# Now we get everything else in
ADD . /app

# Precompile our assets
RUN bin/download-duke-assets || true
ARG SECRET_KEY_BASE=BOGUS
ENV SECRET_KEY_BASE=$SECRET_KEY_BASE
RUN umask 0002; RAILS_ENV=production bundle exec rails assets:precompile

# Set up dirs and permissions so we can run without root
RUN umask 0002; mkdir -p /app/tmp/pids /app/log ;\
    chmod -Rf g+rwx db ; chgrp -R 0 db ;\
    chmod -Rf g+rwx public ; chgrp -R 0 public ;\
    chmod -Rf g+rwx node_modules ; chgrp -R 0 node_modules ;\
    chmod -Rf g+rwx /etc/passwd ; chgrp -R 0 /etc/passwd ;\
    true

RUN ln -s /opt/app/src /app
USER 1000:0

# Args used to provide metadata about the build which is displayed in the web ui
ARG CI_PIPELINE_URL=UNKNOWN
ARG CI_COMMIT_REF_NAME=UNKNOWN
ARG CI_COMMIT_SHA=UNKNOWN
ARG CI_BUILD_TIME=UNKNOWN

ENV CI_PIPELINE_URL=$CI_PIPELINE_URL \
    CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME \
    CI_COMMIT_SHA=$CI_COMMIT_SHA \
    CI_BUILD_TIME=$CI_BUILD_TIME

CMD ["/app/bin/app-start"]
